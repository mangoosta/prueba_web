const app = new Vue({
  el: '#app',
  data: {
    scenarios: {},
    isDiagnosis: true,
    title: 'Diagnosis',
    actualScenario: [],
    node: {}
  },
  methods: {
    toggleState () {
      if (!this.isDiagnosis) {
        this.actualScenario = this.scenarios.diagnosis
        this.title = 'Diagnosis'
        if (this.node) {
          this.node.classList.remove('analysis')
          this.node.classList.remove('no-click')
        }
      }
    },
    toggleScenario (args) {
      this.node = args.classNode

      this.actualScenario = args.isFirstNode
        ? this.scenarios.scenario2 // Peor escenario
        : this.scenarios.scenario1 // Mejor escenario

      this.title = args.isFirstNode
        ? 'Worst Case Scenario'
        : 'Best Case Scenario'

      this.isDiagnosis = false

      let n = (args.classNode.id === '1') ? 0 : 1
      let updateVariables = this.actualScenario[n].variables

      updateVariables = updateVariables.map((variable, index) => {
        if (index === 0) {
         variable.value = args.isFirstNode ? 1 : 0
        } else if (index === updateVariables.length - 1) {
         variable.value = args.isFirstNode ? 0 : 1
        } else {
         variable.value = 0
        }
        return variable
      })

      args.classNode.classList.add('analysis')
      args.classNode.classList.add('no-click')

    }
  },
  created () {
    axios('data.json').then(response => {
      this.scenarios = response.data
      this.actualScenario = this.scenarios.diagnosis
    })
  }
})
