// Barra de probabilidades
Vue.component('bar-p', {
  template: `
    <span
      class="bar"
      :style="{ width: w + 'px', transition: 'all .3s ease' }"
    ></span>
  `,
  props: {
    w: Number
  },
})

// Nodo Clasificador
Vue.component('node-class', {
  template: `
    <div class="node" ref="node">
      <table>
        <caption>{{ caption }}</caption>
        <thead>
          <tr>
            <th colspan="3">Item name</th>
            <th></th>
            <th colspan="4">P (%)</th>
          </tr>
        </thead>
        <tbody>
          <tr
            v-for="row in variables"
            :class="{ 'scenario': row.scenario }"
            @click="handleScenario"
          >
            <td colspan="3">{{ row.item }}</td>
            <td>{{ percentage(row.value) }}</td>
            <td width="100"><bar-p :w="width(row.value)"></bar-p></td>
          </tr>
        </tbody>
      </table>
    </div>
  `,
  props: {
    'caption': String,
    'variables': Array
  },
  data () {
    return {
      toggle: false
    }
  },
  methods: {
    percentage (val) {
      return +(val * 100).toFixed(1)
    },
    width (val) {
      return val * 100
    },
    handleScenario (event) {
      // 1 de 4 posibles escenarios
      const scenarioNode = event.target.parentNode
      // 1 de 2 posibles clases
      const classNode = scenarioNode.parentNode.parentNode.parentNode

      const isFirstNode = (scenarioNode.rowIndex === 1)

      const variables = [...this.$props.variables]

      if ([...scenarioNode.classList].includes('scenario')) {
        const args = { classNode, isFirstNode }
        this.$emit('toggle-scenario', args)
      }
      /*
        let updateVariables = [...this.$props.variables]
        updateVariables = updateVariables.map((variable, index) => {
          if (index === 0) {
            variable.value = isFirstNode ? 1 : 0
          } else if (index === updateVariables.length - 1 ) {
            variable.value = isFirstNode ? 0 : 1
          } else {
            variable.value = 0
          }
          return variable
        })
      */
    }
  }
})
